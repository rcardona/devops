- install non ha
> $ kubectl create ns argocd
>
> $ kubectl apply -f https://raw.githubusercontent.com/argoproj/argo-cd/stable/manifests/install.yaml -n argocd
>

- find credentials
> $ kubectl get secret argocd-initial-admin-secret -n argocd  -o json | jq -r '.data.password' | base64 -d
>

    y16FlKp-Pyqf1Ukw

- expose WebServer
> $ kubectl port-forward svc/argocd-server -n argocd 8080:443
>

- install the cli, documentation [HERE](https://argo-cd.readthedocs.io/en/stable/cli_installation/)
> $ curl -sSL -o /usr/local/bin/argocd https://github.com/argoproj/argo-cd/releases/latest/download/argocd-linux-amd64
>
> $ chmod +x /usr/local/bin/argocd
>

- login to argocd api server
> $ argocd login localhost:8080
>

    ➡ argocd login localhost:8080
    WARNING: server certificate had error: x509: certificate signed by unknown authority. Proceed insecurely (y/n)? y
    Username: admin
    Password:
    'admin:login' logged in successfully
    Context 'localhost:8080' updated

- test connectivity
> $ argocd cluster list
>

- create an application cli
> $ argocd app create app-1 --repo https://gitlab.com/rcardona/devops.git --path argocd/guestbook --dest-server https://kubernetes.default.svc --dest-namespace default
>

- example `guestbook` app, first check the app manifests

      $ cat argocd/guestbook/guestbook-ui-deployment.yaml

      apiVersion: apps/v1
      kind: Deployment
      metadata:
        name: guestbook-ui
      spec:
        replicas: 1
        revisionHistoryLimit: 3
        selector:
          matchLabels:
            app: guestbook-ui
        template:
          metadata:
            labels:
              app: guestbook-ui
          spec:
            containers:
            - image: gcr.io/heptio-images/ks-guestbook-demo:0.2
              name: guestbook-ui
              ports:
              - containerPort: 80

      ----

      $ cat argocd/guestbook/guestbook-ui-svc.yaml

      apiVersion: v1
      kind: Service
      metadata:
        name: guestbook-ui
      spec:
        ports:
        - port: 80
          targetPort: 80
        selector:
          app: guestbook-ui

- creating application with App argocd object

      $ cat manifests/guestbook-application.yaml

      apiVersion: argoproj.io/v1alpha1
      kind: Application
      metadata:
        name: guestbook
        namespace: argocd
      spec:
        destination:
          namespace: guestbook
          server: "https://kubernetes.default.svc"
        project: default
        source:
          repoURL: "https://gitlab.com/rcardona/devops.git"
          path: "argocd/guestbook"
          targetRevision: main
        syncPolicy:
          syncOptions:
            - CreateNamespace=true

- create the argocd application with kubectl
> $ kubectl apply -f https://gitlab.com/rcardona/devops/-/raw/main/manifests/guestbook-application.yaml
>
